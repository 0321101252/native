const Place = require('../models/Place');
const fs = require('fs');
const path = require('path');

exports.getAllPlaces = async (req, res) => {
    try {
        const places = await Place.find();
        res.status(200).json(places);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

exports.getPlaceById = async (req, res) => {
    try {
        const place = await Place.findById(req.params.id);
        if (!place) return res.status(404).json({ message: 'Place not found' });
        res.status(200).json(place);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

exports.createPlace = async (req, res) => {
    const addedPhotos = req.files ? req.files.map(file => '' + file.filename) : [];
    const newPlace = new Place({
        ...req.body,
        addedPhotos: addedPhotos,
    });
    try {
        const savedPlace = await newPlace.save();
        res.status(201).json(savedPlace);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};


exports.updatePlace = async (req, res) => {
    try {
        const place = await Place.findById(req.params.id);
        if (!place) return res.status(404).json({ message: 'Place not found' });

        // Eliminar las imágenes antiguas
        place.addedPhotos.forEach((photo) => {
            fs.unlink(path.join('uploads/', photo), (err) => {
                if (err) console.error(`Failed to delete old photo ${photo}:`, err);
            });
        });

        // Agregar las nuevas imágenes
        const addedPhotos = req.files ? req.files.map(file => '' + file.filename) : [];
        const updateData = {
            ...req.body,
            addedPhotos: addedPhotos,
        };

        const updatedPlace = await Place.findByIdAndUpdate(req.params.id, updateData, { new: true });
        res.status(200).json(updatedPlace);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};


exports.deletePlace = async (req, res) => {
    try {
        await Place.findByIdAndDelete(req.params.id);
        res.status(200).json({ message: 'Place deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

exports.getPlacesByOwner = async (req, res) => {
    try {
        const ownerId = req.params.ownerId; // Asume que el ID del propietario se pasa como parámetro
        const places = await Place.find({ owner: ownerId });
        if (places.length === 0) return res.status(404).json({ message: 'Places not found for this owner' });
        res.status(200).json(places);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};
