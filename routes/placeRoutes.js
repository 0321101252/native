const express = require('express');
const multer = require('multer');
const router = express.Router();
const placeController = require('../controllers/placeController');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/') // Carpeta donde se guardarán las imágenes
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname) // Nombre del archivo
    }
});

const upload = multer({ storage: storage });

router.get('/', placeController.getAllPlaces);
router.get('/:id', placeController.getPlaceById);
router.post('/', upload.array('addedPhotos'), placeController.createPlace);
router.put('/update/:id', upload.array('addedPhotos'), placeController.updatePlace);
router.delete('/:id', placeController.deletePlace);
router.get('/owner/:ownerId', placeController.getPlacesByOwner);

module.exports = router;

