const mongoose = require('mongoose');

const PlaceSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    addedPhotos: [{
        type: String,
    }],
    description: {
        type: String,
        default: "",
        required: true,
    },
    perks: [{
        type: String,
        required: true,
    }],
    extraInfo: {
        type: String,
        default: "",
        required: true,
    },
    checkIn: {
        type: Number,
        default: 14,
        required: true,
    },
    checkOut: {
        type: Number,
        default: 11,
        required: true,
    },
    maxGuests: {
        type: Number,
        default: 1,
    },
    price: {
        type: Number,
        required: true,
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required:true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model('Place', PlaceSchema);
