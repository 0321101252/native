const mongoose = require("mongoose");
const Schema = mongoose.Schema; // Aquí defines Schema

const reservationSchema = new Schema({
    place: { type: Schema.Types.ObjectId, ref: 'Place', required: true },
    checkIn: { type: Date, required: true },
    checkOut: { type: Date, required: true },
    name: { type: String, required: true },
    phone: { type: String, required: true },
    price: { type: Number, required: true },
    client: { type: Schema.Types.ObjectId, ref: 'User', required: true }
}, { timestamps: true });

module.exports = mongoose.model("Booking", reservationSchema);
