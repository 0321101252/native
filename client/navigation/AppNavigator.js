import React from 'react';
import { Animated } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';
import PlacesListScreen from '../screens/PlacesListScreen';
import PlaceDetailScreen from '../screens/PlaceDetailScreen';
import AddEditPlaceScreen from '../screens/AddEditPlaceScreen';
import ProfileScreen from '../screens/ProfileScreen';
import ReservationScreen from '../screens/ReservationFormScreen';
import BookingsListScreen from '../screens/BookingsList';
import BookingDetailScreen from '../screens/BookingDetail';
import OwnerPlacesScreen from '../screens/OwnerPlacesScreen';
import OwnerDetailScreen from '../screens/OwnerDetailScreen';
import OwnerEditPlaceScreen from '../screens/OwnerEditPlace';
import ChartScreen from '../screens/ChartScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const PlacesNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="PlacesList"
      component={PlacesListScreen}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="PlaceDetail"
      component={PlaceDetailScreen}
      options={{ headerTitle: '' }}
    />
    <Stack.Screen
      name="AddEditPlace"
      component={AddEditPlaceScreen}
      options={{ headerTitle: '' }}
    />
    <Stack.Screen
      name="Reservation"
      component={ReservationScreen}
      options={{ headerTitle: '' }}
    />
  </Stack.Navigator>
);

const MainNavigator = () => (
  <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;
        const scaleValue = focused ? 1.3 : 1;
        const scaleStyle = { transform: [{ scale: scaleValue }] };

        if (route.name === 'Profile') {
          iconName = focused ? 'person' : 'person';
        } else if (route.name === 'Places') {
          iconName = focused ? 'home' : 'home';
        } else if(route.name === 'Bookings'){
          iconName = focused ? 'bookmark' : 'bookmark';
        }

        return (
          <Animated.View style={scaleStyle}>
            <MaterialIcons name={iconName} color={color} size={size} />
          </Animated.View>
        );
      },
      tabBarActiveTintColor: 'tomato',
      tabBarInactiveTintColor: 'gray',
      tabBarStyle: [
        {
          display: 'flex',
        },
        null,
      ],
    })}
  >
    <Tab.Screen name="Profile" component={ProfileScreen} options={{ headerShown: false }} />
    <Tab.Screen name="Places" component={PlacesNavigator} options={{ headerShown: false }} />
    <Tab.Screen name="Bookings" component={BookingsNavigator} options={{ headerShown: false }} />
  </Tab.Navigator>
);

const BookingsNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="BookingsList"
      component={BookingsListScreen}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="BookingDetail"
      component={BookingDetailScreen}
      options={{ headerTitle: '' }}
    />
    <Stack.Screen
      name="ChartScreen"
      component={ChartScreen}
      options={{ headerTitle: 'Chart Screen' }} // Puedes personalizar esto según necesites
    />
  </Stack.Navigator>
);

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Main" component={MainNavigator} options={{ headerShown: false }} />
        <Stack.Screen 
          name="OwnerPlacesScreen"
          component={OwnerPlacesScreen}
          options={{ headerTitle: 'Mis Lugares' }}
        />
        <Stack.Screen 
          name="OwnerDetailScreen"
          component={OwnerDetailScreen}
          options={{ headerTitle: 'Detalles del Propietario' }} // personaliza según necesites
        />
        <Stack.Screen 
          name="OwnerEditPlaceScreen"
          component={OwnerEditPlaceScreen}
          options={{ headerTitle: 'Editar Lugar del Propietario' }} // personaliza según necesites
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
