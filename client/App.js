import React from 'react';
import AppNavigator from './navigation/AppNavigator';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import * as eva from '@eva-design/eva';
import { AuthProvider } from './api/authProvider'; // Importa AuthProvider aquí

export default function App() {
  return (
    <AuthProvider>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={eva.light}>
        <AppNavigator />
      </ApplicationProvider>
    </AuthProvider>
  );
}

