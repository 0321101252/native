import React, { useState, useContext } from 'react';
import { Image } from 'react-native';
import { Layout, Text, Button, Card, List } from '@ui-kitten/components';
import api from '../api/index.js';
import { useFocusEffect } from '@react-navigation/native';
import { AuthContext } from '../api/authContext';


const OwnerPlacesScreen = ({ navigation }) => {
    const auth = useContext(AuthContext);
    const [places, setPlaces] = useState([]);
  
    useFocusEffect(
      React.useCallback(() => {
        const { userId } = auth.user;
        const fetchPlaces = async () => {
          try {
            const response = await api.get(`/places/owner/${userId}`);
            setPlaces(response.data);
          } catch (error) {
            console.log('Error al obtener los lugares del propietario', error);
            console.log();
          }
        };
        fetchPlaces();
        return () => {};
      }, [])
    );

  const renderItem = ({ item }) => (
    <Card style={styles.cardContainer}>
      {item.addedPhotos && item.addedPhotos.length > 0 && (
        <Image
          source={{ uri: `http://192.168.1.9:5000/uploads/${item.addedPhotos[0]}` }}
          style={styles.image}
        />
      )}
      <Text category='h6' style={styles.title}>{item.title}</Text>
      <Text category='s1' style={styles.address}>{item.address}</Text>
      <Button
        style={styles.detailButton}
        onPress={() => navigation.navigate('OwnerDetailScreen', { placeId: item._id })}
      >
        Ver detalles
      </Button>
    </Card>
  );

  return (
    <Layout style={styles.layout}>
      <List
        data={places}
        renderItem={renderItem}
      />
    </Layout>
  );
};

const styles = {
  layout: {
    flex: 1,
    padding: 16,
    backgroundColor: '#f5f5f5',
  },
  addButton: {
    marginBottom: 16,
  },
  cardContainer: {
    marginVertical: 8,
    borderRadius: 10,
    overflow: 'hidden',
  },
  image: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginVertical: 8,
  },
  address: {
    fontSize: 16,
    color: '#888',
    marginBottom: 16,
  },
  detailButton: {
    backgroundColor: '#2196F3',
    color: '#ffffff',
  },
};

export default OwnerPlacesScreen;
