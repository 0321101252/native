import React, { useEffect, useState, useContext } from 'react';
import { View, StyleSheet } from 'react-native';
import axios from '../api/index';
import { AuthContext } from '../api/authContext';
import { Layout, Text, Button, Avatar } from '@ui-kitten/components';

const ProfileScreen = ({ navigation }) => {
  const [user, setUser] = useState(null);
  const auth = useContext(AuthContext);  

  useEffect(() => {
    const { userId } = auth.user;

    const fetchUserDetails = async () => {
      try {
        const response = await axios.get(`/users/${userId}`);
        setUser(response.data);
      } catch (error) {
        console.error("Error fetching user details:", error);
        console.log(error.response)
      }
    };

    fetchUserDetails();
  }, []);

  const handleLogout = async () => {
    try {
      await auth.signOut();
      navigation.navigate("Login");
    } catch (error) {
      console.error("Error during logout:", error);
    }
  };

  if (!user) {
    return <Text category="h4">Loading...</Text>;
  }

  return (
    <Layout style={styles.container}>

      <Avatar source={{ uri: user.profileImage }} style={styles.profileImage} />

      <Text category="h5" style={styles.username}>{user.username}</Text>
      <Text category="s1" style={styles.email}>{user.email}</Text>
      
      <View style={styles.buttonContainer}>

        <Button appearance="outline" size="small" style={styles.button} onPress={handleLogout}>Log Out</Button>

        <Button appearance="outline" size="small" style={styles.button} 
        onPress={() => navigation.navigate('Places', { screen: 'AddEditPlace' })}
        >Agregar un nuevo lugar
        </Button>
        
        <Button appearance="outline" size="small" style={styles.button} onPress={() => navigation.navigate('OwnerPlacesScreen', { userId: auth.user.userId })}>
        Ver mis lugares
        </Button>
      </View>

    </Layout>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
  },
  profileImage: {
    width: 120,
    height: 120,
    borderRadius: 60,
    marginBottom: 20,
  },
  username: {
    fontWeight: 'bold',
    fontSize: 24,
    marginBottom: 5,
  },
  email: {
    fontSize: 16,
    color: '#666',
    marginBottom: 30,
  },
  buttonContainer: {
    width: '100%',
    alignItems: 'center',
  },
  button: {
    marginVertical: 5,
    borderRadius: 20,
    width: '60%',
  },
});

export default ProfileScreen;
