import React, { useEffect, useState } from 'react';
import { View, Text, ScrollView, Button } from 'react-native';
import { Card, Layout } from '@ui-kitten/components';
import api from '../api/index';

const BookingDetailScreen = ({ route, navigation }) => {
  const [booking, setBooking] = useState(null);
  const [place, setPlace] = useState(null);

  const goToChartScreen = () => {
    navigation.navigate('ChartScreen', { bookingId: route.params.bookingId });
  };

  useEffect(() => {
    const { bookingId } = route.params;

    const fetchBookingDetails = async () => {
      try {
        const response = await api.get(`/bookings/${bookingId}`);
        setBooking(response.data);

        const placeResponse = await api.get(`/places/${response.data.place}`);
        setPlace(placeResponse.data);
      } catch (error) {
        console.error("Error fetching booking details:", error);
      }
    };

    fetchBookingDetails();
  }, [route.params.bookingId]);

  return (
    <ScrollView>
      <Layout style={{ padding: 10 }}>
        {booking && place ? (
          <>
            <Card style={{ marginVertical: 10, borderRadius: 10 }}>
              <Text category="h5">{place.title}</Text>
              <Text>{place.location}</Text>
              <Text category="h6">Booking Details</Text>
              <Text>Name: {booking.name}</Text>
              <Text>Check-in: {new Date(booking.checkIn).toLocaleDateString()}</Text>
              <Text>Check-out: {new Date(booking.checkOut).toLocaleDateString()}</Text>
              <Text>Price: ${booking.price}</Text>
            </Card>

            <Card style={{ marginVertical: 10, borderRadius: 10 }}>
              <Text category="h5">Place Details</Text>
              <Text>Description: {place.description}</Text>
              <Text>Rooms: {place.rooms}</Text>
              <Text>Beds: {place.beds}</Text>
              <Button title="Go to Chart Screen" onPress={goToChartScreen} />
            </Card>
          </>
        ) : (
          <Text>Loading...</Text>
        )}
      </Layout>
    </ScrollView>
  );
};

export default BookingDetailScreen;
