import React, { useState, useEffect,useContext } from 'react';
import { ScrollView, Image, StyleSheet, View, Text } from 'react-native'; // Agregando Text aquí
import { Input, Button,CheckBox } from '@ui-kitten/components';
import axios from '../api/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as ImagePicker from 'expo-image-picker';
import { AuthContext } from '../api/authContext';


const OwnerEditPlaceScreen = ({ route, navigation }) => {
  const { user } = useContext(AuthContext); // Asegúrate de importar AuthContext
  const userId = user?.userId;

  const availablePerks = [
    { name: 'Wifi', icon: 'wifi' },
    { name: 'Free parking spot', icon: 'car' },
    { name: 'TV', icon: 'television' },
    { name: 'Pets', icon: 'paw' },
  ];

  const placeId = route.params?.placeId;

  const [title, setTitle] = useState('');
  const [address, setAddress] = useState('');
  const [description, setDescription] = useState('');
  const [extraInfo, setExtraInfo] = useState('');
  const [checkIn, setCheckIn] = useState('');
  const [checkOut, setCheckOut] = useState('');
  const [maxGuests, setMaxGuests] = useState('');
  const [price, setPrice] = useState('');
  const [addedPhotos, setAddedPhotos] = useState([]);
  const [selectedPerks, setSelectedPerks] = useState([]);
  const [errorMessage, setErrorMessage] = useState(null);


  const handlePerkChange = (perkName) => {
    if (selectedPerks.includes(perkName)) {
      setSelectedPerks(selectedPerks.filter((item) => item !== perkName));
    } else {
      setSelectedPerks([...selectedPerks, perkName]);
    }
  };

  const handleRemoveImage = (index) => {
    const newAddedPhotos = [...addedPhotos];
    newAddedPhotos.splice(index, 1);
    setAddedPhotos(newAddedPhotos);
  };

  useEffect(() => {
    if (placeId) {
      const fetchPlaceDetails = async () => {
        try {
          const response = await axios.get(`/places/${placeId}`);
          if (response && response.data) { // Añadir una comprobación aquí
            const place = response.data;
            setTitle(place.title);
            setAddress(place.address);
            setDescription(place.description);
            setExtraInfo(place.extraInfo);
            setSelectedPerks(place.perks || []);
            setCheckIn(place.checkIn ? place.checkIn.toString() : '');
            setCheckOut(place.checkOut ? place.checkOut.toString() : '');
            // Convertir los números en cadenas
            setMaxGuests(place.maxGuests ? place.maxGuests.toString() : '');
            setPrice(place.price ? place.price.toString() : '');
            setAddedPhotos(place.addedPhotos);
            console.log("Place details fetched:", place);
          }
        } catch (error) {
          console.error("Error fetching place details:", error);
        }
      };

      fetchPlaceDetails();
    }
  }, [placeId]);

  const selectImage = async () => {
    try {
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
      if (!result.canceled && result.assets && result.assets[0].uri) {
        setAddedPhotos([...addedPhotos, result.assets[0].uri]);
      }
    } catch (error) {
      console.error("Error al seleccionar la imagen:", error);
    }
  };

  const handleSave = async () => {
    const formData = new FormData();
    formData.append('title', title);
    formData.append('address', address);
    formData.append('description', description);
    formData.append('extraInfo', extraInfo);
    formData.append('checkIn', checkIn);
    formData.append('checkOut', checkOut);
    formData.append('maxGuests', parseInt(maxGuests));
    formData.append('price', parseFloat(price));
    formData.append('owner', userId); // Asegúrate de que userId esté disponible en este componente
    console.log("Form data to be sent:", formData);
    selectedPerks.forEach((perk) => {
      formData.append('perks', perk);
    });
    addedPhotos.forEach((photoUri) => {
      formData.append('addedPhotos', {
        uri: photoUri,
        type: 'image/jpeg',
        name: 'image.jpg',
      });
    });
    try {
      if (placeId) {
        console.log("Iniciando la actualización para el ID:", placeId); // Log para saber que la actualización ha comenzado
        await axios.patch(`/places/update/${placeId}`, formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        })
        .then(response => {
          console.log("Actualización exitosa:", response);
          console.log("Datos de la respuesta:", response.data); // Log para ver los datos de la respuesta
          // Aquí puedes manejar la lógica para cuando la actualización sea exitosa
        })
        .catch(error => {
          console.error("Error en la actualización:", error);
          console.error("Detalles del error de red:", error.message, error.config);
          if (error.response) {
            console.error("Datos de error:", error.response.data); // Log para ver los datos de error de la respuesta
          }
          // Aquí puedes manejar la lógica para cuando la actualización falle
        });
      } else {
        console.log("placeId no está definido."); // Log en caso de que placeId no esté definido
      }
              
  
      // Aquí es donde reseteamos la navegación para volver al perfil
      navigation.reset({
        index: 0,
        routes: [
          { name: 'Main', params: { screen: 'Profile', userId: userId } },
        ],
      });
  
    } catch (error) {
      console.error("Error saving place:", error);
      console.error("Response data:", error.response);
      console.log(error.response)
      console.log(error)
      console.log(userId)
      console.log(placeId)
      console.log("Error details:", error);
      if (error.response) {
        console.log("Error response data:", error.response.data);
        console.error("Response data:", error.response.data);
        setErrorMessage(error.response.data.message);
        console.log(error.response)
        console.log(error)
        console.log(userId)
        console.log(placeId)
      } else {
        // puedes manejar el caso en el que no hay respuesta aquí
        setErrorMessage("Ha ocurrido un error desconocido");
      }
    }
  };
  

  return (
    <ScrollView style={styles.container}>

      <Input label="Título" value={title} onChangeText={setTitle} />
      {errorMessage && errorMessage.includes("title") && <Text style={{ color: 'red', marginBottom: 10 }}>El título es requerido.</Text>}

      <Input label="Dirección" value={address} onChangeText={setAddress} />
      {errorMessage && errorMessage.includes("address") && <Text style={{ color: 'red', marginBottom: 10 }}>La dirección es requerida.</Text>}

      <Input label="Descripción" multiline value={description} onChangeText={setDescription} textStyle={{ minHeight: 64 }} />
      {errorMessage && errorMessage.includes("description") && <Text style={{ color: 'red', marginBottom: 10 }}>Descripcion Requerida !.</Text>}

      <Input label="Información Extra" multiline value={extraInfo} onChangeText={setExtraInfo} textStyle={{ minHeight: 64 }} />
      {errorMessage && errorMessage.includes("extraInfo") && <Text style={{ color: 'red', marginBottom: 10 }}>Informacion Extre es requerida !.</Text>}


      <View style={styles.perkContainer}>
        <Text style={styles.label}>Perks:</Text>
        {availablePerks.map((perk, index) => (
          <View style={styles.perkLabel} key={index}>
            <CheckBox
              checked={selectedPerks.includes(perk.name)}
              onChange={() => handlePerkChange(perk.name)}
            />
            <Icon name={perk.icon} size={20} />
            <Text style={styles.perkText}>{perk.name}</Text>
          </View>
        ))}
      </View>

      <Input label="Check-In" value={checkIn} onChangeText={setCheckIn} keyboardType="numeric" />
      {errorMessage && errorMessage.includes("checkIn") && <Text style={{ color: 'red', marginBottom: 10 }}>CheckIn es requerido !.</Text>}

      <Input label="Check-Out" value={checkOut} onChangeText={setCheckOut} keyboardType="numeric" />
      {errorMessage && errorMessage.includes("checkOut") && <Text style={{ color: 'red', marginBottom: 10 }}>CheckOut es requerido !.</Text>}

      <Input label="Número Máximo de Huéspedes" value={maxGuests} onChangeText={setMaxGuests} keyboardType="numeric" />
      {errorMessage && errorMessage.includes("maxGuests") && <Text style={{ color: 'red', marginBottom: 10 }}>El número máximo de huéspedes debe ser un número.</Text>}

      <Input label="Precio por Noche" value={price} onChangeText={setPrice} keyboardType="numeric" />
      {errorMessage && errorMessage.includes("price") && <Text style={{ color: 'red', marginBottom: 10 }}>El precio debe ser un número.</Text>}

      {addedPhotos.map((photoUri, index) => (
        <View key={index} style={styles.imageContainer}>
          <Image source={{ uri: photoUri }} style={styles.image} />
          <Button style={styles.removeImageButton} onPress={() => handleRemoveImage(index)}>Eliminar</Button>
        </View>
      ))}
      <Button style={styles.button} onPress={selectImage}>Select Image</Button>
      <Button style={styles.button} onPress={handleSave}>Guardar</Button>
    </ScrollView>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  image: {
    width: 100,
    height: 100,
    margin: 5,
  },
  button: {
    marginVertical: 5,
  },
  perkContainer: {
    flexDirection: 'row', // Layout en fila
    flexWrap: 'wrap', // Permite que los elementos se ajusten en múltiples líneas si es necesario
    alignItems: 'center', // Alineación vertical
  },
  perkLabel: {
    flexDirection: 'row', // Layout en fila
    alignItems: 'center', // Alineación vertical
    padding: 10, // Relleno
    margin: 5, // Margen
    borderWidth: 1, // Ancho del borde
    borderColor: '#000', // Color del borde
    borderRadius: 25, // Borde redondeado
  },
  perkText: {
    marginLeft: 10, // Margen izquierdo para separar el texto del ícono
  },
  label: { // Estilo para la etiqueta "Perks"
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
    width: '100%', // Asegura que ocupe toda la línea
  },
  imageContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 5,
  },
  removeImageButton: {
    marginLeft: 10,
  },
});

export default OwnerEditPlaceScreen;
