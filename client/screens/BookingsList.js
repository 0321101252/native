import React, { useContext, useEffect, useState } from 'react';
import { View, FlatList, TouchableOpacity } from 'react-native';
import { Layout, Text, Card } from '@ui-kitten/components';
import { useFocusEffect } from '@react-navigation/native';
import { AuthContext } from '../api/authContext';
import api from '../api/index'; // Asegúrate de importar tu archivo API correctamente



const BookingsListScreen = ({ navigation }) => {
    const auth = useContext(AuthContext);
    const [bookings, setBookings] = useState([]);
  
    useFocusEffect(
      React.useCallback(() => {
        const { userId } = auth.user;
  
        const fetchBookings = async () => {
          if (userId) {
            const response = await api.get(`/bookings/user/${userId}`);
            const bookingsWithPlaces = response.data.map(async (booking) => {
              const placeResponse = await api.get(`/places/${booking.place}`);
              return { ...booking, place: placeResponse.data };
            });
  
            const completedBookings = await Promise.all(bookingsWithPlaces);
            setBookings(completedBookings);
          }
        };

        fetchBookings();
      }, [auth.user])
    );

  const goToBookingDetail = (bookingId) => {
    navigation.navigate('BookingDetail', { bookingId });
  };

  return (
    <Layout style={{ flex: 1, padding: 10, marginTop: 15 }}>
      <FlatList
        data={bookings}
        keyExtractor={(item) => item._id}
        renderItem={({ item }) => (
          <Card style={{ borderRadius: 10, marginBottom: 10 }} onPress={() => goToBookingDetail(item._id)}>
            <Text category='h5'>{item.place.title}</Text>
            <Text appearance='hint'>Check In: {new Date(item.checkIn).toLocaleDateString()}</Text>
            <Text appearance='hint'>Check Out: {new Date(item.checkOut).toLocaleDateString()}</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text category='h6'>Total price: </Text>
              <Text category='h6' style={{ fontWeight: 'bold' }}>${item.price}</Text>
            </View>
          </Card>
        )}
      />
    </Layout>
  );
};

export default BookingsListScreen;
