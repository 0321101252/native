import React, { useState, useEffect } from "react";
import { View, Text, ScrollView, StyleSheet } from "react-native";
import api from "../api/index";
import { LineChart } from "react-native-chart-kit";
import { Dimensions } from "react-native";

const ChartScreen = ({ route }) => {
  const [bookingData, setBookingData] = useState(null);
  const { bookingId } = route.params;

  let PData = [];
  let TData = [];
  let RHData = [];
  let LuxData = [];

  if (bookingData && bookingData.data) {
    PData = bookingData.data.map((item) => parseFloat(item.P));
    TData = bookingData.data.map((item) => parseFloat(item.T));
    RHData = bookingData.data.map((item) => parseFloat(item.RH));
    LuxData = bookingData.data.map((item) => parseFloat(item.Lux));
  }


  const chartData = {
    labels: PData.map((_, index) => index.toString()),
    datasets: [
      {
        data: PData,
      },
    ],
  };

  const chartData1 = {
    labels: TData.map((_, index) => index.toString()),
    datasets: [
      {
        data: TData,
      },
    ],
  };

  const chartData2 = {
    labels: RHData.map((_, index) => index.toString()),
    datasets: [
      {
        data: RHData,
      },
    ],
  };

  const chartData3 = {
    labels: LuxData.map((_, index) => index.toString()),
    datasets: [
      {
        data: LuxData,
      },
    ],
  };

  const fetchData = () => {
    console.log("Fetching booking data for ID:", bookingId);
    api
      .get(`/bookings/${bookingId}`)
      .then((response) => {
        console.log("API Response:", response);
        setBookingData(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  };

  useEffect(() => {
    fetchData(); // Llamar a la función una vez al montar el componente

    const interval = setInterval(() => {
      fetchData(); // Llamar a la función cada 5 segundos
    }, 5000);

    return () => {
      clearInterval(interval); // Limpiar el intervalo al desmontar el componente
    };
  }, [bookingId]);

  return (
    <ScrollView>
      <View style={styles.container}>
        {bookingData && bookingData.data ? (
          <>
            <Text style={styles.title}>Gráfica de P</Text>
            <LineChart
              data={chartData}
              width={Dimensions.get("window").width - 20}
              height={220}
              chartConfig={chartConfig}
            />
            <Text style={styles.title}>Gráfica de T</Text>
            <LineChart
              data={chartData1}
              width={Dimensions.get("window").width - 20}
              height={220}
              chartConfig={chartConfig}
            />
            <Text style={styles.title}>Gráfica de RH</Text>
            <LineChart
              data={chartData2}
              width={Dimensions.get("window").width - 20}
              height={220}
              chartConfig={chartConfig}
            />
            <Text style={styles.title}>Gráfica de Lux</Text>
            <LineChart
              data={chartData3}
              width={Dimensions.get("window").width - 20}
              height={220}
              chartConfig={chartConfig}
            />
          </>
        ) : (
          <Text>No existen gráficas para este Screen</Text>
        )}
      </View>
    </ScrollView>
  );
};

const chartConfig = {
  backgroundColor: "#fff",
  backgroundGradientFrom: "#fff",
  backgroundGradientTo: "#fff",
  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
});

export default ChartScreen;
