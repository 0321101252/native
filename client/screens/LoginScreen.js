import React, { useState, useContext, useEffect } from "react";
import { Layout, Text, Input, Button } from "@ui-kitten/components";
import { AuthContext } from "../api/authContext.js";
import api from "../api/index.js";


const LoginScreen = ({ navigation }) => {
  const [message, setMessage] = useState(null);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const auth = useContext(AuthContext);

  useEffect(() => {
    if (!auth.user) {
      // Si el usuario se desloguea, limpia los campos y el mensaje
      setEmail('');
      setPassword('');
      setMessage(null);
    }
  }, [auth.user]);

  const handleLogin = async () => {
    try {
      const response = await api.post("/users/login", {
        email: email,
        password: password,
      });
      if (response.data) {
        console.log("Usuario autenticado exitosamente", response.data);
        setMessage("Usuario autenticado exitosamente!");
        const userToken = response.data.token;
        const userId = response.data.userId; // Accede al userId desde la respuesta
        await auth.signIn({ authToken: userToken, userId: userId });
        navigation.navigate('Main', { screen: 'Profile', params: { userId: userId } });
      }
    } catch (error) {
      console.log("Error durante el inicio de sesión", error);
      console.log("Detalle del error:", error.response);
      setMessage("Email o Contraseña incorrecta !")
    }
  };

  return (
    <Layout style={{ flex: 1, justifyContent: "center", padding: 20 }}>
      <Text category="h1" style={{ textAlign: "center", marginBottom: 20 }}>
        Login
      </Text>
      <Input
        placeholder="Email"
        value={email}
        onChangeText={setEmail}
        style={{ marginBottom: 20 }}
      />
      <Input
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
        secureTextEntry={true}
        style={{ marginBottom: 20 }}
      />
      {message && <Text>{message}</Text>}
      <Button onPress={handleLogin}>Login</Button>
      <Button
        appearance="ghost"
        onPress={() => navigation.navigate("Register")}
      >
        <Text>Don't have an account? Register here.</Text>
      </Button>
    </Layout>
  );
};

export default LoginScreen;
