import React, { useState, useContext } from 'react';
import { Layout, Text, Input, Button, Datepicker, NativeDateService, Modal } from '@ui-kitten/components';
import api from '../api/index.js'; // Asegúrate de utilizar la ruta correcta
import { AuthContext } from '../api/authContext';




const ReservationFormScreen = ({ navigation,route }) => {
  const { user } = useContext(AuthContext);
  const userId = user?.userId;
  const placeId = route.params.placeId; // Asegúrate de pasar 'placeId' como un parámetro en tu navegación

  const [modalVisible, setModalVisible] = useState(false);
  const [arrivalDate, setArrivalDate] = useState(new Date());
  const [departureDate, setDepartureDate] = useState(new Date());
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');

  const getPlaceDetails = async (placeId) => {
    try {
      // Realiza una solicitud GET a la ruta asociada con getPlaceById en tu controlador
      const response = await api.get(`/places/${placeId}`);
      return response.data; // Esto debe coincidir con la estructura de la respuesta de tu API
    } catch (error) {
      console.error('Error al obtener los detalles del lugar:', error);
      console.log(error.response)
      throw error; // Puedes manejar el error según lo necesites en tu aplicación
    }
  };

  const handleConfirmation = () => {
    setModalVisible(false);
    navigation.navigate('PlacesList'); // Navega a la pantalla de lugares
  };

  const handleSubmit = async () => {
    try {
      const checkIn = arrivalDate;
      const checkOut = departureDate;
  
      // Obtener los detalles del lugar para calcular el precio
      const place = await getPlaceDetails(placeId);
      const pricePerNight = place.price;
  
      // Calcular la cantidad total
      const nights = (new Date(checkOut) - new Date(checkIn)) / (1000 * 60 * 60 * 24);
      const price = nights * pricePerNight;
  
      // Construir el objeto de reserva
      const reservationData = {
        place: placeId,
        checkIn: checkIn,
        checkOut: checkOut,
        name: name,
        phone: phone,
        price: price,
        client: userId
      };
  
      // Enviar la solicitud POST
      const response = await api.post('/bookings', reservationData);
  
      // Manejar la respuesta
      console.log('Reserva exitosa:', response.data);
      setModalVisible(true);
    } catch (error) {
      console.error('Error al realizar la reserva:', error);
      console.log(error.response)
    }
  };
  
  

  return (
    <Layout style={{ flex: 1, padding: 20 }}>
      <Text category="h4" style={{ marginBottom: 15 }}>Reservar una estadía</Text>
      <Input label="Nombre" placeholder="Ingresa tu nombre" value={name} onChangeText={setName} style={{ marginBottom: 15 }} />
      <Input label="Teléfono" placeholder="Ingresa tu teléfono" value={phone} onChangeText={setPhone} keyboardType="numeric" style={{ marginBottom: 15 }} />
      <Datepicker label="Fecha de llegada" dateService={new NativeDateService()} date={arrivalDate} onSelect={setArrivalDate} style={{ marginBottom: 15 }} />
      <Datepicker label="Fecha de salida" dateService={new NativeDateService()} date={departureDate} onSelect={setDepartureDate} style={{ marginBottom: 15 }} />
      <Button onPress={handleSubmit} style={{ marginBottom: 15 }}>Hacer Reserva</Button>

      <Modal
        visible={modalVisible}
        backdropStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}
        onBackdropPress={() => setModalVisible(false)}
      >
        <Layout style={{ padding: 20, borderRadius: 10 }}>
          <Text style={{ marginBottom: 15 }}>¿Confirmar la reserva?</Text>
          <Button onPress={handleConfirmation} style={{ marginBottom: 10 }}>Confirmar</Button>
          <Button onPress={() => setModalVisible(false)} status="basic">Cancelar</Button>
        </Layout>
      </Modal>
    </Layout>
  );
};

export default ReservationFormScreen;
