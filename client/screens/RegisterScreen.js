import React, { useState } from 'react';
import { Layout, Text, Input, Button } from '@ui-kitten/components';
import api from '../api/index.js'

const RegisterScreen = ({navigation}) => {
  const [message, setMessage] = useState(null);
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleRegister = async () => {
    try {
      const response = await api.post('/users/register', {
        username: username,
        email: email,
        password: password
      });
      if (response.data) {
        console.log("Usuario registrado exitosamente", response.data);
        setMessage("Usuario registrado exitosamente!");

        // Redirigir al usuario a la pantalla de login después de 3 segundos
        setTimeout(() => {
        navigation.navigate("Login");
        }, 2000);
      }
    } catch (error) {
      console.log("Error durante el registro", error);
    }
  };
  

  return (
    <Layout style={{ flex: 1, justifyContent: 'center', padding: 20 }}>
      <Text category='h1' style={{ textAlign: 'center', marginBottom: 20 }}>Register</Text>
      <Input
        placeholder='Username'
        value={username}
        onChangeText={setUsername}
        style={{ marginBottom: 20 }}
      />
      <Input
        placeholder='Email'
        value={email}
        onChangeText={setEmail}
        style={{ marginBottom: 20 }}
      />
      <Input
        placeholder='Password'
        value={password}
        onChangeText={setPassword}
        secureTextEntry={true}
        style={{ marginBottom: 20 }}
      />
      {message && <Text>{message}</Text>}
      <Button onPress={handleRegister}>
        Register
      </Button>
      <Button
        appearance="ghost"
        onPress={() => navigation.navigate("Register")}
      >
        <Text>Already have an account? Login here.</Text>
      </Button>
    </Layout>
  );
};

export default RegisterScreen;