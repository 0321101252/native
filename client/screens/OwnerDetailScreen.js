import React, { useState } from 'react';
import { ScrollView, Image } from 'react-native';
import { Button, Layout, Text, Divider } from '@ui-kitten/components';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import axios from '../api/index';

const OwnerDetailScreen = ({ route }) => {
    const { placeId } = route.params;
    const navigation = useNavigation();

    if (!placeId) {
        console.error("placeId no está definido!");
        return <Text>Error: ID del lugar no proporcionado.</Text>;
    }

    const [place, setPlace] = useState(null);

    useFocusEffect(
        React.useCallback(() => {
            const fetchPlaces = async () => {
                try {
                    console.log("Iniciando la solicitud para obtener detalles del lugar...");
                    const response = await axios.get(`/places/${placeId}`);
                    setPlace(response.data);
                } catch (error) {
                    console.error("Error al obtener los detalles del lugar:", error);
                    if (error.response) {
                        console.error("Datos de Respuesta del Error:", error.response.data);
                        console.error("Estado de Respuesta del Error:", error.response.status);
                        console.error("Encabezados de Respuesta del Error:", error.response.headers);
                    } else if (error.request) {
                        console.error("Solicitud de Error:", error.request);
                    } else {
                        console.error("Mensaje de Error:", error.message);
                    }
                    console.error("Configuración de Error:", error.config);
                }
            };
    
            fetchPlaces();
            return () => { setPlace(null) }; // función de limpieza
        }, [placeId])
    );
    
    if (!place) {
        return <Text>Cargando detalles...</Text>;
    }

    const handleDelete = async () => {
        try {
            const response = await axios.delete(`/places/${placeId}`);
            navigation.navigate('Main', { screen: 'Places', params: { screen: 'PlacesList' } });
        } catch (error) {
            console.error('Hubo un error al eliminar el lugar:', error);
        }
    };

    return (
        <ScrollView style={styles.container}>
            
            <Layout style={styles.content}>
                <Text category='h4' style={styles.title}>{place.title}</Text>
                <Text category='s1' style={styles.address}>{place.address}</Text>
                <Text style={styles.description}>{place.description}</Text>
                

                <Divider style={styles.divider}/>
                
                {place.addedPhotos.map((photo, index) => (
                <Image 
                   key={index}
                   source={{ uri: `http://192.168.1.9:5000/uploads/${photo}` }}
                   style={styles.image}
                   onError={(e) => console.log(e.nativeEvent.error)}
                />
                    ))}

                <Divider style={styles.divider}/>


                <Text category='h6' style={styles.sectionTitle}>Detalles del alojamiento:</Text>
                <Text style={styles.detailText}>Check-in: {place.checkIn}</Text>
                <Text style={styles.detailText}>Check-out: {place.checkOut}</Text>
                <Text style={styles.detailText}>Máx. Huéspedes: {place.maxGuests}</Text>
                <Text style={styles.detailText}>Precio: ${place.price} por noche</Text>
    
                <Divider style={styles.divider}/>
                
                <Text category='h6' style={styles.sectionTitle}>Perks:</Text>
                <Layout style={styles.perksContainer}>
                    {place.perks.map(perk => (
                        <Text key={perk} style={styles.perkItem}>{perk}</Text>
                    ))}
                </Layout>
                
                <Divider style={styles.divider}/>
                
                <Text category='h6' style={styles.sectionTitle}>Info adicional:</Text>
                <Text style={styles.extraInfo}>{place.extraInfo}</Text>
    
                <Layout style={styles.buttonContainer}>
                <Button style={styles.editButton} onPress={() => navigation.navigate('OwnerEditPlaceScreen', { placeId: placeId })}> Editar</Button>
                <Button style={styles.deleteButton} status='danger' onPress={handleDelete}>Eliminar</Button>
                </Layout>
            </Layout>
        </ScrollView>
    );
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5',
    },
    image: {
        width: '100%',
        height: 200,
        resizeMode: 'cover',
        marginBottom: 16,
    },
    content: {
        padding: 16,
        backgroundColor: '#ffffff',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 8,
    },
    address: {
        fontSize: 16,
        color: '#888',
        marginBottom: 16,
    },
    description: {
        fontSize: 16,
        color: '#666',
        marginBottom: 20,
    },
    divider: {
        marginVertical: 8,
        backgroundColor: '#e0e0e0',
    },
    sectionTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 16,
        marginBottom: 8,
    },
    detailText: {
        fontSize: 16,
        color: '#777',
        marginBottom: 8,
    },
    perksContainer: {
        paddingLeft: 8,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    perkItem: {
        marginBottom: 8,
        marginRight: 8,
        backgroundColor: '#f0f0f0',
        padding: 4,
        borderRadius: 4,
    },
    extraInfo: {
        marginTop: 16,
        fontSize: 16,
        color: '#555',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 24,
    },
    backButton: {
        flex: 0.45,
        backgroundColor: '#2196F3',
        color: '#ffffff',
    },
    deleteButton: {
        flex: 0.45,
        backgroundColor: '#f44336',
        color: '#ffffff',
    },
};

export default OwnerDetailScreen;