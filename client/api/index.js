import axios from 'axios';

const api = axios.create({
  baseURL: 'http://192.168.1.9:5000' // Esto asume que tu servidor Express está corriendo en el puerto 5000.
});

export default api;
