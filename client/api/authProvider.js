import React, { useState, useEffect } from 'react';
import { AuthContext } from './authContext';
import { getToken, removeToken, storeToken } from './auth';

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);

  // Intenta recuperar el token al iniciar la aplicación
  useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await getToken();
      } catch (e) {
        console.error("Restoring token failed", e);
      }

      // Si hay un token, puedes recuperar los detalles del usuario aquí si es necesario

      setUser(userToken);
      setLoading(false);
    };

    bootstrapAsync();
  }, []);

  const authContext = {
    signIn: async (data) => {
      // Aquí puedes llamar a tu API para iniciar sesión y obtener el token
      const userToken = data.authToken;
      const userId = data.userId;
  
      await storeToken({ authToken: userToken, userId });
      setUser({ authToken: userToken, userId });
    },
    signOut: async () => {
      await removeToken();
      setUser(null);
    },
    user,
  };

  return (
    <AuthContext.Provider value={authContext}>
      {loading ? null : children}
    </AuthContext.Provider>
  );
};
