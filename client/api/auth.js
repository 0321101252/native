import AsyncStorage from '@react-native-async-storage/async-storage';

export const storeToken = async (tokenInfo) => {
  try {
    await AsyncStorage.setItem('authTokenInfo', JSON.stringify(tokenInfo));
  } catch (error) {
    console.error("Error al guardar la información de autenticación:", error);
  }
};

export const getToken = async () => {
  try {
    const tokenInfo = await AsyncStorage.getItem('authTokenInfo');
    return JSON.parse(tokenInfo);
  } catch (error) {
    console.error("Error al recuperar la información de autenticación:", error);
  }
};

export const removeToken = async () => {
  try {
    await AsyncStorage.removeItem('authTokenInfo');
  } catch (error) {
    console.error("Error al eliminar la información de autenticación:", error);
  }
};

