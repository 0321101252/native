const multer = require('multer');
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const placeRoutes = require('./routes/placeRoutes.js');
const userRoutes = require('./routes/userRoutes.js');
const bookingRoutes = require('./routes/bookingRoutes.js');

const app = express();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/') // Carpeta donde se guardarán las imágenes
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname) // Nombre del archivo
    }
});

const upload = multer({ storage: storage });

app.post('/upload', upload.single('addedPhotos'), (req, res) => {
    // Aquí puedes manejar el archivo subido
    // req.file contiene información sobre el archivo
    // req.body contiene cualquier otro dato enviado en la petición
  
    // Por ejemplo, puedes responder con la URL de la imagen
    res.json({ imageUrl: '/uploads/' + req.file.filename });
});

// Middleware
app.use(cors());
app.use(express.json());

// Conexión a MongoDB
mongoose.connect('mongodb+srv://admin:e51584d345@cluster0.perlxcn.mongodb.net/merndb', { //mongodb://localhost:27017/merndb  mongodb+srv://Alan:AlanElMisa123@alanclouster.actp261.mongodb.net/merndb

    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.once('open', () => {
    console.log('>>> DB Is Connected');
});

// Rutas
app.use('/places', placeRoutes);
app.use('/users', userRoutes);
app.use('/bookings', bookingRoutes); // Ruta para las reservas
app.use('/uploads', express.static('uploads'));

const PORT = 5000;
app.listen(PORT, () => {
    console.log(`Servidor corriendo en http://localhost:${PORT}`);
});